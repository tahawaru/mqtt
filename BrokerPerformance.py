#!/usr/bin/env python3

import paho.mqtt.client as mqtt
import time
import sys
import json
import os
import psutil

def cpu_usage():
    return psutil.cpu_times().user

# ----------- CONNECTION -----------

# This is the Subscriber
#broker = "12.0.0.246" # - raspberry Pi
broker = "localhost"#"12.0.0.170" # - broker/gw
#broker = "12.0.0.138" # - pc1
topic="physical_usage"
port = 1882

# ----------- Global variables -----------

timeout = 5 # if no messages are received for timeout seconds, the program stops
start = 0
elapsed = 0
received_start = 0
received_finish = 0
cpu_start = 0
cpu_finish = 0

# ----------- WHAT HAPPENS WHEN MESSAGE IS RECEIVED -----------

def on_message(client, userdata, message):
    global cpu_start    
    global cpu_finish
    global received_start
    global received_finish
    payload = str(message.payload.decode("utf-8"))
    print(payload)
        
    if payload == "start":
        received_start+=1
        if received_start :
            cpu_start = cpu_usage()
            #print("Inicial cpu = " + str(cpu_start))
    if payload == "finish":
        received_finish+=1

client = mqtt.Client("Broker Performance Tester")
client.on_message = on_message
client.connect(broker, port)
client.subscribe(topic)
start = time.time()
time.clock()
print("Subscribed, now waiting for messages on topic 'physical_usage' ...")
#If no message arives before timeout, starts analisys
while True:
    client.loop()
    #print(received_start)
    #print(received_finish)
    if received_finish == received_start and received_start != 0:
        client.loop_stop()
        cpu_finish = cpu_usage()
        #print("Final cpu = " + str(cpu_finish))
        break

cpu_spent = cpu_finish - cpu_start
print("CPU spent was: " + str(cpu_spent))

f = open("physical_usage.txt","a+")
f.write("%s\n" % str(cpu_spent))
f.close()
----- USAGE OF THE MQTT TEST CASE WITH MOSQUITTO BROKER -----

1) Run mosquitto on port 1882:

Open a terminal and execute the following command: "mosquitto -p 1882"

This will start the mosquitto service listening on port 1882 (localhost).

Note: The usage of port 1882 is made because thingsboard is also 
active and by default thingsboard and rabbitmq and mosquitto 
listens to the same port which is 1883.

Troubleshooting: 

If the service doesn't start use "sudo service mosquitto status" 
to see if it is already running on the background, if it is 
stop it with "sudo service mosquitto stop" and execute step 1) again.

2) Subscribe:

 - Open a new terminal and go to the path of the subscriber 
   script: "cd ~/Desktop/mqtttestcase"
	
   Run the script with: "python mqttsubs.py"

   If you want to change the name of the timestamps file 
   use for example: "python mqttsubs.py ttc_1.1" or use 
   the default one "ttc_file"
   
   Note that the subscriber script has a timeout of 5s which can be 
   changed in the file.
   Don't forget to change the host and port of the broker on the script.

3) Publish:

 - Open a new terminal and go to the path of the publisher 
   script: "cd ~/Desktop/mqtttestcase"
	
   Run the script with: "python mqttgen.py"
   If you want the messages to contain a bigger payload as 7KB
   you can use for example: "python mqttgen.py 1000"
   
   Note that if you want to send different number of messages 
   or want a different interarrivel time between each message 
   you must change the values in the script as well as for the
   host and port of the broker.

4) Get the report:

 - Open a new terminal and go to the path of the monitor 
   script: "cd ~/Desktop/mqtttestcase"

   For this script to work, both "sender.txt" and "receiver.txt"
   must be on the same path and machine as the script.

   To easily have them both on the same path and machine you can 
   send the files needed to the monitor path using for example the 
   following commands:

   "sudo scp receiver_log.txt ttc_1.1.txt siti@12.0.0.170:~/Desktop/mqtttestcase"
   "sudo scp sender_log.txt siti@12.0.0.170:~/Desktop/mqtttestcase"

   Note that these are just an example, you must change the ip of 
   the machine you chose to use as well as the directory and name 
   of the files.

   After having the files in the same path and machine you can
   run the monitor script to obtain the final report:

   "python monitor.py" - report will have the name "monitor.txt"
	or
   "python monitor.py reportA" - report will have the name "reportA.txt"

   The final report shows the number of messages sent by the publisher,
   the number of messages received by the subscriber, the time to completion
   as an average of all messages sent and received and for last,
   the packet loss in percentage, in this order.

5) Repeat all 4 steps above for each experimentation you want to perform.


Note: 

There is also a script called "BrokerPerformance.py" which porpuse is to subscribe
to topic "physical_usage". The script "mqttgen.py" also sends to this topic a message
when it starts and when it finishes, so that the "BrokerPerformance.py" knows when to 
the experiment started and finished. With this information, the "BrokerPerformance.py"
checks the hole machine cpu consumption at start and end of the experiment. The subctration
of this values will give us the cpu consumption of the hole experiment in the 
machine that runs the broker. To test this script, simply run it after the Mosquitto
broker is started and before the subscriber.
#!/usr/bin/env python3
"""a simple sensor data generator that sends to an MQTT broker via paho"""
import sys
import json
import time
from random import randint
import paho.mqtt.client as mqtt
import os
import psutil

def cpu_usage():
    return psutil.cpu_times().user

def utf8len(s):
    return len(s.encode('utf-8'))

#Clean log file
if os.path.exists("sender.txt"):
    os.remove("sender.txt")

""" 
    Sends data to specified broker at specific rate for a determined amount of time
    At the end builds a log file with some values that are needed to make an analysis.
"""
mqttc = mqtt.Client("Publisher")
broker = "12.0.0.170" #"localhost"
port = 1882
topic = "sensors"
topic2 = "physical_usage"

mqttc.connect(broker, port)

interarrival_time = 1000 #in miliseconds
start = time.time()
time.clock()
num_msgs_send = 500
msgs_sent = 0
mqttc.loop_start()
cpu_usage_start = 0
cpu_usage_finish = 0

for i in range(num_msgs_send):
    #print("#Msgs: " + str(i))
    x= time.sleep(interarrival_time/1000.0)
    msg = "testing" #default message contains 7 bytes, one for each char
    if len(sys.argv) > 1:
        msg = ""
        interval = randint(7, int(sys.argv[1]))
        for i in range(interval):
            msg += "x"
    #print(msg) 
    msg_size = utf8len(str(msg))
    #print("Msg size: " + str(msg_size))

    data = {
        "temperature": msg,
        "timestamp": (time.time() * 1000), # time function returns in sec, so * 1000 to get mili sec
    }

    payload = json.dumps(data)
    mqttc.publish(topic, payload)
    msgs_sent += 1
    if msgs_sent == 1:
        cpu_usage_start = cpu_usage()
        mqttc.publish(topic2, "start")
cpu_usage_finish = cpu_usage()
mqttc.publish(topic2, "finish")
cpu_usage = cpu_usage_finish - cpu_usage_start
mqttc.loop_stop()
#create a file with msg_sent
f = open("sender.txt","w+")
f.write("%d\n" % msgs_sent)
#f.write("%.3f\n" % cpu_usage)
f.close()
print("Messages sent: " + str(msgs_sent))
print("Cpu usage is: " + str(cpu_usage))